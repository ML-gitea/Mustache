
<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php';
$mustache = new Mustache_Engine([
        'loader' => new Mustache_Loader_FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/../mustache/')
    ]
);
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>

    <?
        $cards = [
            'commonTitle' => 'Главный <br>заголовок',
            'title' => 'Второй заголовок',
            'cards' => [
                [
                    'title' => 'Карточка 1',
                    'description' => 'Интересное описание карточки.Интересное описание карточки.Интересное описание карточки.',
                    'date' => 'Сегодня',
                    'image' => 'NewPhoto.png'
                ],
                [
                    'description' => 'Интересное описание карточки.Интересное описание карточки.Интересное описание карточки.',
                    'date' => '01.01.1999'
                ],
                [
                    'title' => 'Карточка 3',
                    'description' => 'Интересное описание карточки.Интересное описание карточки.Интересное описание карточки.',
                    'date' => '24 сентября 2022',
                    'image' => 'Object.jpg'
                ],
            ],
        ]
    ?>

    <? echo $mustache->render('cards-list', $cards) ?>

</body>
</html>
